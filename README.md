# salesforce-integration

![salesforce integration](https://gitlab.com/sfdcamit/salesforce-integration/-/blob/master/Hero%20Image%20-%20Integration.jpg)

As we are living in a world where everything is related to each other and collaboration is a key thing to succeed. 

This course is designed to learn Salesforce Integration from the very beginning and then advanced to Advance Salesforce Integration. In this course, you will be able to learn every basic information about Salesforce Integration.

This course leverage all types of Authentication that can be used while integration with any third-party system including API Key, Basic, OAuth 2.0, OAuth 1.0, JWT &, etc.

In this course, you will also learn how to use different API REST & SOAP, which is best in which case.

At the end of the course, you will be able to write any kind of integration for sure.

Note: - The course contains a Major project to Integrate Salesforce with the ActiveCompaign system.

Bonus:- 

- You will get the PPT of each lecture
- Code related to each lecture
- New Videos will be added in the coming future related to other integration topics
- Plenty no of Assignments for you to practice the integration
- List of 20+ API to integrate with Salesforce
- Live Project implementation along with me
- Course Completion Certification

Here is the link of the unmanaged packge which can be used to setup all the Objects inside salesforce. [Udemy Integration](packaging/installPackage.apexp?p0=04t2x0000048Isi)

You can use [This Link](https://gitlab.com/sfdcamit/salesforce-integration/-/tree/master/Code) to access the Code which we have developed through out the course.

You can use [This Link](https://gitlab.com/sfdcamit/salesforce-integration/-/tree/master/PPT) to access the PPT.

You can use [This Link](https://gitlab.com/sfdcamit/salesforce-integration/-/tree/master/JsForce%20Demo) to access the JavaScript code which we have used for Subscribing the events outside of salesforce.
